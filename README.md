# 2D Triangulation

Este repositorio consta básicamente de un script llamado **_triang.py_** que
realiza una triangulación (para nada ángulo-óptima) en 2D según una serie de
puntos ingresados. Este README es solo una descripción básica de su uso. Si
quiere una descripción detallada sobre cómo funciona **_triang_** por dentro
[clic aquí](https://docs.google.com/presentation/d/1XjBpTA6FZIkJSWR6aZ0jUyqJCBJBu4zjWX6RhuYatWs/edit?usp=sharing).

## Setup

Para usar este script basta tener Python instalado en su versión 3.6.9 (al
menos), la cual es la más estable hasta ahora. Además de esto, debemos tener las
 siguientes librerías instaladas:

 - numpy (usada para el manejo de determinantes de matrices, algunos arreglos
      y también para la generación de enteros aleatorios).
  - matplotlib (visualización de la malla triangular).
  - time (debería venir instalada junto a Python).
  - csv (encargada de abrir, leer y escribir archivos csv)

## Uso

Su uso es bastante fácil. Consta de dos inputs iniciales justo después de
ejecutar el programa en cualquier terminal o IDE:

    $ Ingrese un numero de ptos. aleatorios: # Aquí ingresar cantidad de ptos que
                                # se quieran
    $ Ingrese un limite superior: # Aqui ingresamos el numero maximo que queremos
                                # que aparezca como coordenada de nuestros ptos.
                                # Además, ayuda a generar el canvas de nuestra
                                # malla triangular.
    $ Quiere que el dibujo sea interactivo (y/n)?: # Se responde _y_ si se
                                # quiere que se muestre un dibujo paso a paso,
                                # o solo la triangulación final (_n_).

El segundo y tercer inputes exactamente lo que se comentó después de los "#".
Ahora bien, según lo que ingresemos en el primer **input** (el de la cantidad
de ptos. aleatorios), habrán diferentes formas de ingresar los puntos a
triangular en nuestra malla. También es necesario aclarar que los números
manejados por **_triang_** deberían ser enteros. En futuras versiones de este
algoritmo, se podría cambiar esto a que también maneje operaciones de puntos
flotantes. Por ahora, por simplicidad, se mantiene este estado.

### 1. Ingresar _0_

Al ingresar _0_, básicamente le estamos diciendo a **_triang_** que no
queremos puntos aleatorios, sino que ingresaremos nuestros puntos de forma
interactiva uno a uno mediante la consola.

El formato de lo que hay que entregar es: _2,3_ . Esto indica al programa que
queremos ingresar el punto con coordenadas 2 en **x** y 3 en **y** a nuestra triangulación. Solo podemos ingresar un string de esa forma. Si no lo hacemos,
el programa no lo reconocerá y seguramente mostrará una excepción/error que hará
que termine.

Ejemplo:

    $ Punto: 2,3
    $ Punto ingresado correctamente.

Para dejar de ingresar puntos y que el programa deje de ejecutarse, debe
escribir la palabra _end_ y verse además una frase. Ejemplo:

    $ Punto: end
    $
    $ Disfrute sus triangulaciones! Bye bye nwn.

Y debería quedarse con al menos una imagen en matplotlib con su triangulación
hecha. Quedará con dos ventanas de matplotlib si es que activó el dibujo paso
a paso de la triangulación.

### 2. Ingresar un _x > 0_

Si elegimos ingresar cualquier número **x** mayor a 0, **_triang_** entenderá
que queremos generar una malla triangular con puntos generados de forma
aleatoria. Estos puntos son generados usando la función **np.random.randint()**
que genera un número (o una matriz, o un arreglo) aleatorios (de números
aleatorios) perteneciente al intervalo **[x0, x1)** donde **x0** puede aparecer y
**x1** no (excluye a **x1**). La cantidad que ingresamos es la cantidad de ptos.
aleatorios que generará el programa. Aunado a esto, cabe destacar que la forma
en como serán elegidos las coordenadas de estos puntos está dada por una
distribución de probabilidad uniforme entre los números límites escogidos.

### 3. Ingresar un _x < 0_

Finalmente, si ingresamos un número menor a cero, **_triang_** entenderá que
queremos cargar un archivo _.csv_ con puntos descritos. Para poder leer este
archivo habrá que especificar su nombre sin añadirle la extensión _.csv_,
además, el archivo deberá estar estructurado de la siguiente forma:

![Formato de archivo de puntos .csv](https://imgur.com/XcjfLpW.png)

Además, la consola le pedirá en su momento ingresar el nombre del archivo de
la siguiente forma, y como ejemplo, se responde solo con el nombre del archivo
como se dijo anteriormente. Recordar que el archivo debe estar en la misma
carpeta que **_triang.py_**.

    $ Ingrese nombre de archivo de puntos: prueba

Si el archivo no está en el formato descrito, **_triang_** no podrá extraer
correctamente la información y tendrá que ejecutarse nuevamente con un archivo
que sí esté en el formato correcto.

Entonces, cuando especifique todos estos parámetros ya descritos, el programa
debería empezar a mostrar un dibujo interactivo (o no).

### Extra

  - Si usted eligió ingresar un número de puntos aleatorios o ingresó un archivo
  _.csv_, de todas maneras el **_triang_** le pedirá por medio de un input si
  quiere agregar más puntos a mano. Si no quiere, recuerde que lo que debe hacer
  es entregarle _end_ a ese input. Para más información referirse a la opción
  **1** de **Uso**.

  - Si quiere hacer más lento el dibujo paso a paso, solo debe buscar la palabra
  _sleep_ en el código y descomentarlo. Podemos usar _sleep()_ (función de Time)
  con el parámetro que esté acorde a nuestros deseos. Quizás en alguna edición
  futura, este programa pida este parámetro como input de usuario, pero al no
  ser estrictamente necesario, no se implementó esta funcionalidad.

  - Al final de la ejecución, **_triang_** también le printeará una lista con
  todos los triángulos formados. Los triángulos con _None_ son triángulos que
  estuvieron en la lista de triángulos, sin embargo, fueron desreferenciados
  porque ya no iban a estar más en uso. v1[x, y] representa un vértice de un
  triángulo con su coordenada x e y respectivamente. Veremos triángulos así:

    $ Triangulo _numero del triángulo en la lista_: v1[x, y] v2[x, y] v3[x, y]

### Ejemplos de ploteo

#### Interactivo:
![Ploteo interactivo de la triangulación 2D](https://i.imgur.com/ecX9l90.png)

#### No interactivo:
![Ploteo simple de la triangulación 2D](https://imgur.com/firVbm2.png)
